 #metodos y los atributos privados


class User:

    def __init__(self, username, ident, password):
        self.username = username
        self.ident = ident
        self.__password = password   # atributo privado, no puede ser instanciado directamente


user1 = User ('Carlos', '124568', '654825')
user2 = User ('daniel', '563492', '312548')

print(user1.username)
print(user2.__password)
print "aqui estoy en la rama master"
ddd